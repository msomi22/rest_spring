
package co.ke.appletech.server.spring;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//import org.hibernate.validator.constraints.NotEmpty;

/** 
 * An object that can be persisted with its primary key as an uuid.
 * <p>
 * 
 *
 */
@MappedSuperclass
public class StorableBeanByUUID extends StorableBean {

	@Id
	@Column(name = "uuid", unique = true)
    //@NotEmpty
	private String uuid;
	
	private String accountId;
        
	
	/**
	 * 
	 */
	public StorableBeanByUUID() {
		uuid = UUID.randomUUID().toString();
		accountId = "";
	}
		
	
	

	public String getUuid() {
		return uuid;
	}




	public void setUuid(String uuid) {
		this.uuid = uuid;
	}




	public String getAccountId() {
		return accountId;
	}




	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}




	/**
	 * 
	 */
	private static final long serialVersionUID = 6321411695612782172L;
}
