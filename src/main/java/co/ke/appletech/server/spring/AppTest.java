/**
 * 
 */
package co.ke.appletech.server.spring;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 
 * @author peter
 *
 */
public class AppTest extends TestCase {

	public void testApp() {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory(); 
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Staff staff = new Staff();
		staff.setAccountId("E3CDC578-37BA-4CDB-B150-DAB0409270CD");
		staff.setAcessLevelId("615F04C1-00BF-499C-AC7A-B46B69243AAA");
		staff.setIsActive("1");
		staff.setFirstname("Peter");
		staff.setMiddlename("Mwenda");
		staff.setLastname("Njeru");
		
		session.save(staff);

		session.getTransaction().commit();
		session.close();
	}
}