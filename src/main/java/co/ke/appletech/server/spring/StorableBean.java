
package co.ke.appletech.server.spring;

import java.io.Serializable;
import java.util.Random;

/***
 * This class represents an object in the School System architecture that can be
 * stored in the RDBMS as well as cached.
 * 
 * @author <a href="mailto:mwendapeter72@gmail.com">Peter mwenda</a>
 *
 */
public class StorableBean implements Serializable {

	/**
	 * 
	 */
	public static final long serialVersionUID = new Random().nextLong();
}